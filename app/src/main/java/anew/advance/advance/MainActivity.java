package anew.advance.advance;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import anew.advance.advance.custom_views.MyImageView;
import anew.advance.advance.utils.BaseActivity;
import anew.advance.advance.utils.BaseApplication;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((MyImageView)findViewById(R.id.img))
                .load("http://media.irib.ir/assets//radio_slider/20180819080816_2466.png");
    }


}
