package anew.advance.advance.utils;

import android.app.Application;
import android.graphics.Typeface;

public class BaseApplication extends Application {
    static BaseApplication baseApp ;

    public static Typeface typeFace;

    @Override
    public void onCreate() {
        super.onCreate();
        baseApp = this ;
        typeFace =
                Typeface.createFromAsset(getAssets()
                , Constants.appFontName);
    }
}
